<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress-dev' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-T_O.xC,3p>qI,AVB#, YP$.`X/+jrn_sQjCr~o&70=Chlk`#A:+<{fhnO.>_|go');
define('SECURE_AUTH_KEY',  '@.6]g| q<%]pqRZN2e>LDvJ8|FpG|k-}EY=D&zRvI!)D&<OEv:p,nv}{UTI3!t}a');
define('LOGGED_IN_KEY',    '{4Eah>fjvUMW-YhKgK.A M]Ec(J; PL-@^E|hw9~lNoVw^RLwGvK]kH)H)91Qt3L');
define('NONCE_KEY',        'LE&}%_SPRmsxMOZe&#49-8%aBK<Hane1|9n-F7%Gg[m0,#2X+O>!X#ng*+YXMc~b');
define('AUTH_SALT',        'G}+3.|r_q]~ h&8nr[E?9VaixyazGaDw-uhw>+.G[#V0K;?f@vcWo+SBb8T6=QP2');
define('SECURE_AUTH_SALT', '/S7e5Q~D7A`l6J^e|}qo-,,}c[B(l1Q-o+DvD.~yx6p3B6_Wg7t[:&/KaU_Xs[6y');
define('LOGGED_IN_SALT',   '<2.]80L#KP{XL/$h| ?LdB_S;3[h81EG]OeOOw#KdvI.Eip|Bbs>yD~si 6?VZZf');
define('NONCE_SALT',       'hwRe0h:oCd*?Xo8>oz<KYlT-+nB0]M|d bbu<>e$@P%][bGV,is...4c)*>cNfbD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
