<?php
/**
 * Plugin Name:       Hooks example
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       A basic hooks example
 * Version:           1.0
 * Author:            Jeff Reeve
 * Author URI:        https://jeffreeve.co.uk
 * Text Domain:       hooks
 */

 function ju_hooks($title){
    return "hooked " . $title;
 }

 function ju_footer_shoutout(){
   echo "Hooked action example v1<br>";
 }

 function ju_footer_shoutout_v2(){
  echo "Hooked actionexample v2<br>";
}

 add_filter('the_title', 'ju_hooks');
 add_action('wp_footer', 'ju_footer_shoutout');
 add_action('wp_footer', 'ju_footer_shoutout_v2', 5);

 